//
//  Alg.swift
//  DistinctHSL
//
//  Created by Nir Levi on 06/10/2019.
//  Copyright © 2019 Crystal. All rights reserved.
//

import Foundation
import UIKit



class Alg {
    
    let K : CGFloat = 0.2
    let STEP : Int = 57
    
    func HSLGradual (colors : Int) -> ColorsDataModel{
        let colorsDataModel = ColorsDataModel()
        var currentDegree = Int.random(in: 0...360)
        var h = CGFloat(currentDegree)/CGFloat(360);
        var s = CGFloat(K)
        var l : CGFloat = 1
        
        var colorCount = colors
        var steps : Int = 0
        var rot : Int = 0
        var degree : Int = 0
        let totalRots : Int = colors / (360 /  STEP)
        print("totalRots:\(totalRots)")
        while colorCount>0 {
            degree = steps * STEP
            currentDegree = degree % 360
            rot = degree / 360
            colorsDataModel.appendColor(theColor: UIColor(hue: h, saturation: s, brightness: l, alpha: 1.0))
            h = CGFloat(currentDegree)/CGFloat(360)
            s = 1 - CGFloat(rot) / CGFloat(CGFloat(totalRots) * 1.5)
            l = CGFloat.random(in: 0.3...1)
            colorCount -= 1
            steps += 1
        }
        
        return colorsDataModel
    }
    
}
