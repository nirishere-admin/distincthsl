//
//  colorsDataModel.swift
//  DistinctHSL
//
//  Created by Nir Levi on 05/10/2019.
//  Copyright © 2019 Crystal. All rights reserved.
//

import Foundation
import UIKit

class ColorsDataModel {
    
    var arr : [UIColor] = []
    func appendColor(theColor: UIColor){
        arr.insert(theColor, at: 0)
    }
    
    var count : Int {
        get {
            return arr.count
        }
    }
    
    func retrieveColor() -> UIColor? {
        if(arr.count>0){
            let range = 0...arr.count-1
            let key = Int.random(in: range)
            let theColor = arr.remove(at: key)
            return theColor
        }else{
            return nil
        }
    }
}
