//
//  ViewController.swift
//  DistinctHSL
//
//  Created by Nir Levi on 04/10/2019.
//  Copyright © 2019 Crystal. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let algo = Alg();
    var colorsDataModel : ColorsDataModel?
    @IBOutlet weak var nTextField: UITextField!
    @IBOutlet weak var goButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func goClicked(_ sender: Any) {
        let n = Int(nTextField.text!)
        if(n! > 0){
            performSegue(withIdentifier: "segueShowColors", sender: self)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segueShowColors"){
            if(nTextField.text != ""){
                let destinationViewController = segue.destination as! ColorsViewController
                let n = Int(nTextField.text!)
                destinationViewController.colorsDataModel = algo.HSLGradual(colors: n!)
            }
        }
    }
    
}

