//
//  ColorsViewController.swift
//  DistinctHSL
//
//  Created by Nir Levi on 05/10/2019.
//  Copyright © 2019 Crystal. All rights reserved.
//

import UIKit

class ColorsViewController: UIViewController {

    @IBOutlet weak var colorsView: UIView!
    @IBOutlet weak var pencilButton: UIButton!
    
    var colorsDataModel : ColorsDataModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        drawColors()
    }
    

    func drawCircle(color : UIColor, atX : Int, atY : Int, radius: Int){
        let circleLayer = CAShapeLayer();
        let newColor = color
        circleLayer.fillColor = newColor.cgColor
        circleLayer.path = UIBezierPath(ovalIn: CGRect(x: atX, y: atY, width: radius, height: radius)).cgPath;
        colorsView.layer.addSublayer(circleLayer)
    }
    
    @IBAction func pencilClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getScreenWidth()->CGFloat{
        return UIScreen.main.bounds.width
    }
    func getScreenHeight()->CGFloat{
        return UIScreen.main.bounds.height
    }
    
    func drawColors(){

        let cnt : Int = (colorsDataModel?.count)!
        let x : Int = Int(CGFloat(cnt).squareRoot().rounded(.down))
        let y : Int = cnt / x
        
        let odd : Bool = cnt > x*y
        let xStep : CGFloat = getScreenWidth()/CGFloat(x);
        let yStep : CGFloat = getScreenHeight()/CGFloat(y + (odd && y>1 ? 1 : 0));

        for j in 0..<(y + (odd ? 1 : 0)) {
            for i in 0..<x {
                let color = colorsDataModel?.retrieveColor()
                if(color) != nil{
                    drawCircle(color: color!, atX: Int(xStep)*i - Int(xStep/2), atY: Int(yStep)*j - Int(yStep/3) , radius: min(Int(xStep*2.5),Int(yStep*2.5)))
                }
            }
        }
    }
    
}
